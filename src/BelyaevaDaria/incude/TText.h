#ifndef __TTEXT_H_
#define __TTEXT_H_

#include <stack>
#include <string>
#include <fstream>
#include "TTextLink.h"
#include "TDataCom.h"

class TText;
typedef TText *PTText;

class TText : public TDataCom {
protected:
    PTTextLink pFirst;      // указатель корня дерева
    PTTextLink pCurrent;      // указатель текущей строки
    stack< PTTextLink > Path; // стек траектории двжение
    stack< PTTextLink > St;   // стек ля итератора
    PTTextLink GetFirstAtom(PTTextLink pl); // поиск первого атома
    void PrintText(PTTextLink ptl);         // печать текста со звена ptl
    void PrintTextFile(PTTextLink ptl, ofstream& TxtFile);
    PTTextLink ReadText(ifstream &TxtFile); // чтение текста из файла
public:
    TText(PTTextLink pl = NULL);
    ~TText() { pFirst = NULL; }

    int GoFirstLink(void); // переход к первой строке
    int GoDownLink(void);  // переход к следующей строке по Down
    int GoNextLink(void);  // переход к следующей строке по Next
    int GoPrevLink(void);  // переход к предыдущей позиции в тексте

    string GetLine(void);   // чтение текущей строки
    void SetLine(string s); // замена текущей строки

    void InsDownLine(string s);    // вставка строки в подуровень
    void InsDownSection(string s); // вставка раздела в подуровень
    void InsNextLine(string s);    // вставка строки в том же уровне
    void InsNextSection(string s); // вставка раздела в том же уровне
    void DelDownLine(void);        // удаление строки в подуровне
    void DelDownSection(void);     // удаление раздела в подуровне
    void DelNextLine(void);        // удаление строки в том же уровне
    void DelNextSection(void);     // удаление раздела в том же уровне
                     
    int Reset(void);              // установить на первую запись
    int IsTextEnded(void) const;  // проверка на завершенность
    int GoNext(void);             // перехо к следующей записи
    
    void Read(char * pFileName);  // ввод текста из файла
    void Write(char * pFileName); // вывод текста в файл

    void Print(void);             // печать текста
};

#endif