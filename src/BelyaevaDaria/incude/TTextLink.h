#ifndef __TTEXTLINK_H
#define __TTEXTLINK_H

#define _CRT_SECURE_NO_WARNINGS

#include "TDatValue.h"
#include <iostream>
#include <string>

#define TextLineLength 50
#define MemSize 50

using namespace std;

class TText;
class TTextLink;
typedef TTextLink *PTTextLink;
typedef char TStr[TextLineLength];

class TTextMem {
    PTTextLink pFirst;
    PTTextLink pLast;
    PTTextLink pFree;
    friend class TTextLink;
};
typedef TTextMem *PTTextMem;

class TTextLink: public TDatValue {
protected:
    TStr Str;
    PTTextLink pNext, pDown;
    static TTextMem MemHeader;
public:
    static void InitMemSystem(int size = MemSize);  //инициализация памяти
    static void PrintFreeLink(void);  //печать свобоных звеньев
    void * operator new (size_t size); //выеление звена
    void operator delete (void *pM);   //освобождение звена
    static void MemCleaner(TText &txt); //сборка мусора
    TTextLink(TStr s = NULL, PTTextLink pn = NULL, PTTextLink pd = NULL) {
        pNext = pn;
        pDown = pd;
        if (s != NULL)
            strcpy_s(Str, s);
        else
            Str[0] = '\0';
    }
    TTextLink(string s) {
        pNext = nullptr; pDown = nullptr;
        strcpy_s(Str, s.c_str());
    }
    ~TTextLink() {}
    int IsAtom() { return pDown == NULL; } //проверка атомарности звена
    PTTextLink GetNext() { return pNext; }
    PTTextLink GetDown() { return pDown; }
    PTDatValue GetCopy() { return new TTextLink(Str, pNext, pDown); }
protected:
    virtual void Print(ostream &os) { os << Str; }

friend class TText;
};

#endif