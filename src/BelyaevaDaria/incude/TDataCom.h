#ifndef __TDATACOM_H_
#define __TDATACOM_H_

#define TextOK	0
#define TextNoDown 101
#define TextNoNext 102
#define TextNoPrev 103
#define TextError -102
#define TextNoMem -101

class TDataCom {
protected:
    int RetCode;       
    int SetRetCode(int ret) { return RetCode = ret; }
public:
    TDataCom() : RetCode(TextOK) { }
    virtual ~TDataCom() = 0 { }
    int GetRetCode() {
        int temp = RetCode;
        RetCode = TextOK;
        return temp;
    }
};

#endif