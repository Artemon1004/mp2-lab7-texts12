#ifndef __TDATVALUE_H
#define __TDATVALUE_H

class TDatValue;
typedef TDatValue *PTDatValue;

class TDatValue {
    public:
        virtual TDatValue * GetCopy() = 0;
        ~TDatValue() {}   
};

#endif
