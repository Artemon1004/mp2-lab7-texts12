#include "TText.h"

int main(){
    TTextLink::InitMemSystem();
    TText text;
    text.Read("input.txt");
    text.GoFirstLink();
    text.GoDownLink();
    text.DelDownSection();
    text.InsDownSection("1.1.1");
    TTextLink::MemCleaner(text);
    text.Print();
    text.Write("output.txt");
}