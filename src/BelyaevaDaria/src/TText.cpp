#define _CRT_SECURE_NO_WARNINGS

#include <conio.h>
#include "TText.h"

#define BufLength 100

static char StrBuf[BufLength + 1]; // буфер для ввода строк
static int TextLevel; // номер текущего уровня текста

TText::TText(PTTextLink pl) {
	if (pl == NULL)
		pl = new TTextLink();
	pFirst = pl;
}

int TText::GoFirstLink(void) {
	while (!Path.empty())
		Path.pop();
	pCurrent = pFirst;
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else
		SetRetCode(TextOK);
	return RetCode;
}

int TText::GoDownLink(void) {
	SetRetCode(TextError);
	if (pCurrent != NULL)
		if (pCurrent->pDown != NULL) {
			Path.push(pCurrent);
			pCurrent = pCurrent->pDown;
			SetRetCode(TextOK);
		}
		return RetCode;
}

int TText::GoNextLink(void) {
	SetRetCode(TextError);
	if (pCurrent != NULL)
		if (pCurrent->pNext != NULL) {
			Path.push(pCurrent);
			pCurrent = pCurrent->pNext;
			SetRetCode(TextOK);
		}
		return RetCode;
}

int TText::GoPrevLink(void) {
	if (Path.empty())
		SetRetCode(TextNoPrev);
	else {
		pCurrent = Path.top();
		Path.pop();
		SetRetCode(TextOK);
	}
	return RetCode;
}

string TText::GetLine(void) {
	if (pCurrent == NULL)
		return string("");
	else
		return string(pCurrent->Str);
}

void TText::SetLine (string s) {
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else
		strncpy(pCurrent->Str, s.c_str(), TextLineLength);
	pCurrent->Str[TextLineLength - 1] = '\0';
}

void TText::InsDownLine(string s) {
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else {
		PTTextLink pd = pCurrent->pDown;
		PTTextLink pl = new TTextLink("", pd, NULL);
		strncpy(pl->Str, s.c_str(), TextLineLength);
		pl->Str[TextLineLength - 1] = '\0';
		pCurrent->pDown = pl;
		SetRetCode(TextOK);
	}
}

void TText::InsDownSection(string s) {
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else {
		PTTextLink pd = pCurrent->pDown;
		PTTextLink pl = new TTextLink("", NULL, pd);
		strncpy(pl->Str, s.c_str(), TextLineLength);
		pl->Str[TextLineLength - 1] = '\0';
		pCurrent->pDown = pl;
		SetRetCode(TextOK);
	}
}

void TText::InsNextLine(string s) {
	if (pCurrent == NULL)
		SetRetCode(TextError);
    else {
        TStr buf;
        strcpy_s(buf, s.c_str());
        pCurrent->pNext = new TTextLink(buf, pCurrent->pNext, NULL);
        SetRetCode(TextOK);
    }
}

void TText::InsNextSection(string s) {
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else {
        TStr buf;
        strcpy_s(buf, s.c_str());
        pCurrent->pNext = new TTextLink(buf, NULL, pCurrent->pNext);
		SetRetCode(TextOK);
	}
}

void TText::DelDownLine(void) {
	SetRetCode(TextOK);
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else if (pCurrent->pDown != NULL) {
		PTTextLink pl1 = pCurrent->pDown;
		PTTextLink pl2 = pl1->pNext;
		if (pl1->pDown == NULL)
			pCurrent->pDown = pl2;
	}
}

void TText::DelDownSection(void) {
	SetRetCode(TextOK);
	if (pCurrent == NULL)
		SetRetCode(TextError);
	else if (pCurrent->pDown != NULL) {
		PTTextLink pl1 = pCurrent->pDown;
		PTTextLink pl2 = pl1->pNext;
		pCurrent->pDown = pl2;
	}
}

void TText::DelNextLine(void) {
    if (pCurrent == NULL)
        SetRetCode(TextError);
    else if (pCurrent->pNext == NULL)
        SetRetCode(TextNoNext);
    else if (pCurrent->pNext->IsAtom())
        pCurrent->pNext = pCurrent->pNext->pNext;
}

void TText::DelNextSection(void) {
    if (pCurrent == NULL)
        SetRetCode(TextError);
    else if (pCurrent->pNext == NULL)
        SetRetCode(TextNoNext);
    else
        pCurrent->pNext = pCurrent->pNext->pNext;
}

int TText::Reset(void) {
	while (!St.empty())
		St.pop();
	pCurrent = pFirst;
	if (pCurrent != NULL) {
		St.push(pCurrent);
		if (pCurrent->pNext != NULL)
			St.push(pCurrent->pNext);
		if (pCurrent->pDown != NULL)
			St.push(pCurrent->pDown);
	}
	return IsTextEnded();
}

int TText::IsTextEnded(void) const {
	return !St.size();
}

int TText::GoNext(void) {
	if (!IsTextEnded()) {
		pCurrent = St.top();
		St.pop();
		if (pCurrent != pFirst) {
			if (pCurrent->pNext != NULL)
				St.push(pCurrent->pNext);
			if (pCurrent->pDown != NULL)
				St.push(pCurrent->pDown);
		}
	}
	return IsTextEnded();
}

PTTextLink TText::GetFirstAtom(PTTextLink pl) {
	PTTextLink tmp = pl;
	while (!tmp->IsAtom()) {
		St.push(tmp);
		tmp = tmp->GetDown();
	}
	return tmp;
}


void TText::Print() {
	TextLevel = 0;
	PrintText(pFirst);
}

void TText::PrintText(PTTextLink ptl) {
	if (ptl != NULL) {
		for (int i = 0; i < TextLevel; i++)
			cout << " ";
        cout << ptl->Str << endl;
		TextLevel++; PrintText(ptl->GetDown());
		TextLevel--; PrintText(ptl->GetNext());
	}
}

void TText::PrintTextFile(PTTextLink ptl, ofstream & TxtFile) {
    if (ptl != NULL) {
        for (int i = 0; i < TextLevel; i++)
            TxtFile << " ";
        TxtFile << ptl->Str << endl;
        TextLevel++; PrintTextFile(ptl->GetDown(), TxtFile);
        TextLevel--; PrintTextFile(ptl->GetNext(), TxtFile);
    }
}

void TText::Read(char* pFileName) {
	ifstream TxtFile(pFileName);
	TextLevel = 0;
	if (&TxtFile != NULL)
		pFirst = ReadText(TxtFile);
}

PTTextLink TText::ReadText(ifstream& TxtFile) {
    string buf;
    PTTextLink ptl = new TTextLink();
    PTTextLink tmp = ptl;
    while (!TxtFile.eof()) {
        getline(TxtFile, buf);
        if (buf.front() == '}')
            break;
        else if (buf.front() == '{')
            ptl->pDown = ReadText(TxtFile);
        else {
            ptl->pNext = new TTextLink(buf.c_str());
            ptl = ptl->pNext;
        }
    }
    ptl = tmp;
    if (tmp->pDown == NULL) {
        tmp = tmp->pNext;
        delete ptl;
    }
    return tmp;
}

void TText::Write(char * pFileName) {
    TextLevel = 0;
    ofstream TextFile(pFileName);
    PrintTextFile(pFirst, TextFile);
}