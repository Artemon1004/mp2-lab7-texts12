#include "../include/TTextLink.h"
#include "../include/TText.h"

TTextMem TTextLink::MemHeader;

void TTextLink::InitMemSystem(int size) {
    char* tmp = new char[sizeof(TTextLink) * size]{ NULL };
    MemHeader.pFirst = (PTTextLink)tmp;
    MemHeader.pLast = (PTTextLink)tmp + size - 1;
    MemHeader.pFree = MemHeader.pFirst;
    for (PTTextLink link = MemHeader.pFirst; link != MemHeader.pLast; link++)
        link->pNext = link + 1;
    MemHeader.pLast->pNext = NULL;
}

void * TTextLink::operator new(size_t size){
    if (MemHeader.pFree == NULL)
        return NULL;
    PTTextLink free = MemHeader.pFree;
    MemHeader.pFree = free->pNext;
    free->pNext = NULL;
    return free;
}

void TTextLink::operator delete(void* pM) {
    PTTextLink link = (PTTextLink)pM;
    link->pNext = MemHeader.pFree;
    MemHeader.pFree = link;
}

void TTextLink::MemCleaner(TText &txt) {
    txt.Reset();
    while (!txt.IsTextEnded())
        txt.GetCurrentLink()->SetMarked(true);
    PTTextLink link = MemHeader.pFree;
    while (link != NULL) {
        link->SetMarked(true);
        link = link->GetNext();
    }
    link = MemHeader.pFirst;
    for (int i = 0; i < MEM_SIZE; i++) {
        if (link->IsMarked())
            link->SetMarked(false);
        else {
            delete link;
        }
        link++;
    }
}

void TTextLink::PrintFreeLink() {
    for (PTTextLink link = MemHeader.pFree; link != NULL; link = link->GetNext())
        cout << link->Str << endl;
}