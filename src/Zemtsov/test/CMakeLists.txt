set(target "tests")

file(GLOB srcs "*.cpp")

#add_library(library STATIC ${SOURCE_LIB})
add_executable(${target} ${srcs} ${hdrs})

target_link_libraries(${target} src ${LIBRARY})
target_link_libraries(${target} gtest ${LIBRARY})