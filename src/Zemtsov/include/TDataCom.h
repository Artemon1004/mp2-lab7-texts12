#pragma once

#define OK 0
#define NO_ATOM 1
#define ERROR_NO_TEXT_LINK 2
#define ERROR_NO_FIRST_TEXT_LINK 3
#define ERROR_NO_CURRENT_TEXT_LINK 4
#define PATH_IS_EMPTY 5

class TDataCom {
protected:

    int _Message;

public:

    TDataCom() : _Message(OK) {}

    void SetMessage(int message) {
        _Message = message;
    }

    int GetMessage() {
        int m = _Message;
        _Message = OK;
        return m;
    }

};