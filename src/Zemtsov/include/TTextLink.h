#pragma once

#define TEXT_LINE_LENGTH 1000
#define MEM_SIZE 1000

#include <iostream>
#include "TDatValue.h"

class TTextLink;
typedef TTextLink *PTTextLink;
typedef char TStr[TEXT_LINE_LENGTH];

class TTextMem {
    PTTextLink pFirst;     // ��������� �� ������ �����
    PTTextLink pLast;      // ��������� �� ��������� �����
    PTTextLink pFree;      // ��������� �� ������ ��������� �����
    friend class TTextLink;
};
typedef TTextMem *PTTextMem;

class TText;

class TTextLink : public TDatValue {
protected:
    TStr Str;  // ���� ��� �������� ������ ������
    PTTextLink pNext, pDown;  // ��������� �� ���. ������� � �� ����������
    static TTextMem MemHeader; // ������� ���������� �������
    bool Marked = false;
public:
    void SetMarked(bool state) { Marked = state; }
    bool IsMarked() { return Marked; }
    static void InitMemSystem(int size = MEM_SIZE); // ������������� ������
    static void PrintFreeLink(void);  // ������ ��������� �������
    void * operator new (size_t size); // ��������� �����
    void operator delete (void *pM);   // ������������ �����
    static void MemCleaner(TText &txt); // ������ ������
    TTextLink(TStr s = NULL, PTTextLink pn = NULL, PTTextLink pd = NULL) {
        pNext = pn; pDown = pd;
        CopyStr(s);
    }
    TStr* GetStr() {
        return &Str;
    }
    void CopyStr(TStr s) {
        if (s != NULL)
            strcpy(Str, s);
        else
            Str[0] = '\0';
    }
    ~TTextLink() {}
    int IsAtom() { return pDown == NULL; } // �������� ����������� �����
    PTTextLink GetNext() { return pNext; }
    PTTextLink GetDown() { return pDown; }
    PTDatValue GetCopy() { return new TTextLink(Str, pNext, pDown); }
protected:
    virtual void Print(std::ostream &os) { os << Str; }
    friend class TText;
};