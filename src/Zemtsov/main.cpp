#include <iostream>
#include "include/TText.h"

int main(int argc, char** argv) {
    TTextLink::InitMemSystem();
    std::cout << "\nLoading file example: \n";
    TText text;
    text.Read("ourfile.txt");
    text.Print();
    getchar();
    return 0;
}