#include "../include/TText.h"

#include <fstream>
#include <string>
#include <cstring>

PTTextLink TText::GetFirstAtom(PTTextLink pl) {
    while (pl != NULL && !pl->IsAtom())
        pl = pl->GetDown();
    if (pl == NULL)
        _Message = NO_ATOM;
    return pl;
}

void TText::PrintText(PTTextLink ptl, int textLevel) {
    if (ptl != NULL) {
        for (int i = 0; i < textLevel; i++)
            cout << '\t';
        cout << ptl->Str << endl;
        textLevel++;
        PrintText(ptl->GetDown(), textLevel);
        textLevel--;
        PrintText(ptl->GetNext(), textLevel);
    }
}

void TText::PrintToFile(PTTextLink ptl, int textLevel, ofstream& file) {
    if (ptl != NULL) {
        for (int i = 0; i < textLevel; i++)
            file << '\t';
        file << ptl->Str << endl;
        textLevel++;
        PrintToFile(ptl->GetDown(), textLevel, file);
        textLevel--;
        PrintToFile(ptl->GetNext(), textLevel, file);
    }
    else
        _Message = ERROR_NO_TEXT_LINK;
}

PTTextLink TText::ReadText(ifstream &TxtFile) {
    string buf;
    PTTextLink pt = new TTextLink();
    PTTextLink tmp = pt;
    while (!TxtFile.eof()) {
        getline(TxtFile, buf);
        if (buf.front() == '}')
            break;
        else if (buf.front() == '{')
            pt->pDown = ReadText(TxtFile);
        else {
            char* b = new char[TEXT_LINE_LENGTH];
            strcpy(b, buf.c_str());
            pt->pNext = new TTextLink(b);
            pt = pt->pNext;
        }
    }
    pt = tmp;
    if (tmp->pDown == nullptr) {
        tmp = tmp->pNext;
        delete pt;
    }
    return tmp;
}

TText::TText(PTTextLink pl) {
    if (pl == NULL)
        pFirst = new TTextLink();
    else
        pFirst = pl;
    pCurrent = pFirst;
}

PTText TText::GetCopy() {
    PTTextLink link = new TTextLink();
    CopyLink(pFirst, link);
    return new TText(link);
}

PTTextLink TText::CopyLink(PTTextLink linkFrom, PTTextLink linkTo) {
    if (linkFrom == NULL || linkTo == NULL) {
        _Message = ERROR_NO_TEXT_LINK;
        return NULL;
    }
    linkTo->CopyStr(linkFrom->Str);
    if (linkFrom->GetDown() != NULL) {
        linkTo->pDown = new TTextLink();
        CopyLink(linkFrom->GetDown(), linkTo->GetDown());
    }
    if (linkFrom->GetNext() != NULL) {
        linkTo->pNext = new TTextLink();
        CopyLink(linkFrom->GetNext(), linkTo->GetNext());
    }
    return linkTo;
}

int TText::GoFirstLink(void) {
    if (pFirst != NULL) {
        pCurrent = pFirst;
        while (!Path.empty())
            Path.pop();
    }
    else
        return ERROR_NO_FIRST_TEXT_LINK;
    return GetMessage();
}

int TText::GoDownLink(void) {
    if (pCurrent == NULL)
        _Message = ERROR_NO_CURRENT_TEXT_LINK;
    else if (pCurrent->GetDown() != NULL) {
        Path.push(pCurrent);
        pCurrent = pCurrent->GetDown();
    }
    return GetMessage();
}

int TText::GoNextLink(void) {
    if (pCurrent == NULL)
        _Message = ERROR_NO_CURRENT_TEXT_LINK;
    else if (pCurrent->GetNext() != NULL) {
        Path.push(pCurrent);
        pCurrent = pCurrent->GetNext();
    }
    return GetMessage();
}

int TText::GoPrevLink(void) {
    if (!Path.empty()) {
        pCurrent = Path.top();
        Path.pop();
    }
    else
        _Message = PATH_IS_EMPTY;
    return GetMessage();
}

string TText::GetLine(void) {
    if (pCurrent == NULL) {
        _Message = ERROR_NO_CURRENT_TEXT_LINK;
        return NULL;
    }
    return pCurrent->Str;
}

void TText::SetLine(string s) {
    if (pCurrent != NULL)
        strcpy(pCurrent->Str, s.c_str());
    else
        _Message = ERROR_NO_CURRENT_TEXT_LINK;
}

void TText::InsDownLine(string s) { // вставка строки в подуровень
    if (pCurrent != NULL) {
        TStr buf;
        strcpy(buf, s.c_str());
        pCurrent->pDown = new TTextLink(buf, pCurrent->pDown, NULL);
    }
    else
        _Message = ERROR_NO_CURRENT_TEXT_LINK;
}

void TText::InsDownSection(string s) { // вставка раздела в подуровень
    if (pCurrent != NULL) {
        TStr buf;
        strcpy(buf, s.c_str());
        pCurrent->pDown = new TTextLink(buf, NULL, pCurrent->pDown);
    }
    else
        _Message = ERROR_NO_CURRENT_TEXT_LINK;
}

void TText::InsNextLine(string s) { // вставка строки в том же уровне
    if (pCurrent != NULL) {
        TStr buf;
        strcpy(buf, s.c_str());
        pCurrent->pNext = new TTextLink(buf, pCurrent->pNext, NULL);
    }
    else
        _Message = ERROR_NO_CURRENT_TEXT_LINK;
}

void TText::InsNextSection(string s) { // вставка раздела в том же уровне
    if (pCurrent != NULL) {
        TStr buf;
        strcpy(buf, s.c_str());
        pCurrent->pNext = new TTextLink(buf, NULL, pCurrent->pNext);
    }
    else
        _Message = ERROR_NO_CURRENT_TEXT_LINK;
}

void TText::DelDownLine(void) { // удаление строки в подуровне
    if (pCurrent == NULL)
        _Message = ERROR_NO_CURRENT_TEXT_LINK;
    else if (pCurrent != NULL && pCurrent->pDown != NULL)
        if (pCurrent->pDown->IsAtom())
            pCurrent->pDown = pCurrent->pDown->pNext;
}

void TText::DelDownSection(void) { // удаление раздела в подуровне
    if (pCurrent != NULL)
        pCurrent->pDown = NULL;
    else
        _Message = ERROR_NO_CURRENT_TEXT_LINK;
}

void TText::DelNextLine(void) { // удаление строки в том же уровне
    if (pCurrent == NULL)
        _Message = ERROR_NO_CURRENT_TEXT_LINK;
    else if (pCurrent != NULL && pCurrent->pNext != NULL)
        if (pCurrent->pNext->IsAtom())
            pCurrent->pNext = pCurrent->pNext->pNext;
}

void TText::DelNextSection(void) { // удаление раздела в том же уровне
    if (pCurrent != NULL)
        pCurrent->pNext = pCurrent->pNext->pNext;
    else
        _Message = ERROR_NO_CURRENT_TEXT_LINK;
}

int TText::Reset(void) { // установить на первую звапись
    GoFirstLink();
    if (pCurrent != NULL) {
        if (pCurrent->pDown != NULL)
            St.push(pCurrent->pDown);
        if (pCurrent->pNext != NULL)
            St.push(pCurrent->pNext);
    }
    return OK;
}

int TText::IsTextEnded(void) const {
    return St.empty();
}

int TText::GoNext(void) {
    if (!IsTextEnded()) {
        pCurrent = St.top();
        St.pop();
        if (pCurrent != pFirst) {
            if (pCurrent->pNext != NULL)
                St.push(pCurrent->pNext);
            if (pCurrent->pDown != NULL)
                St.push(pCurrent->pDown);
        }
    }
    return IsTextEnded();
}

void TText::Read(char * pFileName) {
    ifstream file(pFileName);
    if (file)
        pFirst = ReadText(file);
}

void TText::Write(char * pFileName) {
    ofstream file(pFileName);
    PrintToFile(pFirst, 0, file);
}

void TText::Print(void) {
    PrintText(pFirst, 0);
}