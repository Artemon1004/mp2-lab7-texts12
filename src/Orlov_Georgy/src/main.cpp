#include "TText.h"

int main()
{
  
  TTextLink::InitMemSystem(14);
  TText text;
  text.Read("input.txt");

  text.GoFirstLink();                // wow 1
  text.GoDownLink();                 // wow 1.1
  text.GoDownLink();                 // wow 1.1.1
  text.GoDownLink();                 // wow 1.1.1.1

  text.DelNextLine();                // del wow 1.1.1.2
  
  text.GoFirstLink();                 // wow 1
  text.GoNextLink();                  // wow 2
  text.DelDownSection();              // del wow 2.1, 2.2, 2.2.1, 2.3

  text.InsNextLine("wow 3.1.1");  // new wow 3.1.1
  text.InsNextSection("wow 3");   // new wow 3

  // No memory
  TTextLink::MemCleaner(text);

  // Print delete node
  TTextLink::PrintFreeLink();
 
  text.GoFirstLink();                  // Session 1
  text.GoNextLink();                   // Session 2
  text.GoNextLink();                   // Session 3
  text.InsDownSection("wow 3.1");  // new Session 3.1

  text.Print();
  text.Write("output.txt");
}