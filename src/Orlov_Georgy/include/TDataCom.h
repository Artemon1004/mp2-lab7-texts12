#pragma once

// ��������� ����� ����������
#define TextOk 0
#define TextError 1
#define TextNoMem 2
#define TextNoDown 3
#define TextNoNext 4
#define TextNoPrev 5

// TDataCom �������� ����� ������� �������
class TDataCom
{
protected:
  int RetCode; // ��� ����������
  int SetRetCode(int ret) { return RetCode = ret; }
public:
  TDataCom(): RetCode(TextOk) {}
  virtual ~TDataCom() = 0 {}
  int GetRetCode()
  {
    int temp = RetCode;
    RetCode = TextOk;
    return temp;
  }
};
