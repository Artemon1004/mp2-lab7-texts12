#pragma once

#include "TDatValue.h"
#include <iostream>

#define TextLineLength 40

using namespace std;

class TText;
class TTextLink;

typedef TTextLink *PTTextLink;
typedef char TStr[TextLineLength];

class TTextMem
{
    PTTextLink pFirst;     // ��������� �� ������ �����
    PTTextLink pLast;      // ��������� �� ��������� �����
    PTTextLink pFree;      // ��������� �� ������ ��������� �����
    TTextMem() : pFirst(nullptr), pLast(nullptr), pFree(nullptr) {};
    friend class TTextLink;
};
typedef TTextMem *PTTextMem;

class TTextLink : public TDatValue
{
protected:
    TStr Str;  // ���� ��� �������� ������ ������
    PTTextLink pNext, pDown;  // ��������� �� ���. ������� � �� ����������
    static TTextMem MemHeader; // ������� ���������� �������
public:
    static void InitMemSystem(int size); // ������������� ������
    static void PrintFreeLink(void);  // ������ ��������� �������
    void * operator new (size_t size); // ��������� �����
    void operator delete (void *pM);   // ������������ �����
    static void MemCleaner(TText &txt); // ������ ������
    TTextLink(TStr s = nullptr, PTTextLink pn = nullptr, PTTextLink pd = nullptr)
    {
        pNext = pn; pDown = pd;
        if (s != nullptr) strcpy(Str, s); else Str[0] = '\0';
    }
    TTextLink(string s)
    {
        pNext = nullptr; pDown = nullptr;
        strcpy_s(Str, s.c_str());
    }
    ~TTextLink() {}
    int IsAtom() { return pDown == nullptr; } // �������� ����������� �����
    PTTextLink GetNext() { return pNext; }
    PTTextLink GetDown() { return pDown; }
    PTDatValue GetCopy() { return new TTextLink(Str, pNext, pDown); }
protected:
    virtual void Print(ostream &os) { os << Str; }
    friend class TText;
};
