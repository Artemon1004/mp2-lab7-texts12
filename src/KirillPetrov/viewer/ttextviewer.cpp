#include "ttext.h"

int main()
{
  
  TTextLink::InitMemSystem(14);
  TText text;
  text.Read("input.txt");

  text.GoFirstLink();                // Session 1
  text.GoDownLink();                 // Session 1.1
  text.GoDownLink();                 // Session 1.1.1
  text.GoDownLink();                 // Session 1.1.1.1

  text.DelNextLine();                // del Session 1.1.1.2
  
  text.GoFirstLink();                 // Session 1
  text.GoNextLink();                  // Session 2
  text.DelDownSection();              // del Session 2.1, 2.2, 2.2.1, 2.3

  text.InsNextLine("Session 3.1.1");  // new Session 3.1.1
  text.InsNextSection("Session 3");   // new Session 3

  // No memory
  TTextLink::MemCleaner(text);

  // Print delete node
  TTextLink::PrintFreeLink();
 
  text.GoFirstLink();                  // Session 1
  text.GoNextLink();                   // Session 2
  text.GoNextLink();                   // Session 3
  text.InsDownSection("Session 3.1");  // new Session 3.1

  text.Print();
  text.Write("output.txt");
}