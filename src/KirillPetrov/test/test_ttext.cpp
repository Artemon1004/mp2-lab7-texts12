#include "gtest.h"
#include "ttext.h"
#include <locale>

#include <iostream>

TEST(TText, can_create_a_text_parameter)
{
  // Arg
  const string str = "Hello world!";
  
  TTextLink Link = TTextLink(str);
  PTTextLink pLink = &Link;
  
  TTextLink::InitMemSystem(1);
  TText T (pLink);

  // Act
  T.GoFirstLink();

  // Assert
  EXPECT_EQ(T.GetLine(), str);
}

TEST(TText, can_add_down_a_line)
{
  // Arg
  TTextLink::InitMemSystem(2);
  TText T;

  // Act
  const string str = "Hello world!";
  T.GoFirstLink();
  T.InsDownLine(str);
  T.GoDownLink();

  // Assert
  EXPECT_EQ(T.GetLine(), str);
}

TEST(TText, can_change_the_current_line)
{
  // Arg
  TTextLink::InitMemSystem(2);
  TText T;

  // Act
  T.GoFirstLink();
  const string str = "Hello world!";
  T.SetLine(str);

  // Assert
  EXPECT_EQ(T.GetLine(), str);
}

TEST(TText, can_add_go_down_a_line)
{
  // Arg
  TTextLink::InitMemSystem(2);
  TText T;

  // Act
  const string str = "Hello world!";
  T.GoFirstLink();
  T.InsDownLine(str);
  T.GoPrevLink();
  T.GoDownLink();

  // Assert
  EXPECT_EQ(T.GetLine(), str);
}

// �������� ������ ���������

TEST(TText, can_see_an_error_in_the_absence_of_down)
{
  // Arg
  TTextLink::InitMemSystem(2);
  TText T;

  // Act & assert
  T.GoFirstLink();
  EXPECT_EQ(T.GoDownLink(), Data::TextNoDown);
}

TEST(TText, can_see_an_error_in_the_absence_of_prev)
{
  // Arg
  TTextLink::InitMemSystem(2);
  TText T;

  // Act & assert
  T.GoFirstLink();
  EXPECT_EQ(T.GoPrevLink(), Data::TextNoPrev);
}

TEST(TText, can_see_an_error_in_the_absence_of_next)
{
  // Arg
  TTextLink::InitMemSystem(2);
  TText T;

  // Act & assert
  T.GoFirstLink();
  EXPECT_EQ(T.GoNextLink(), Data::TextNoNext);
}

TEST(TText, can_bring_a_positive_error_code)
{
  // Arg
  const string str = "Hello world!";
  TTextLink::InitMemSystem(2);
  TText T;
  T.GoFirstLink();
  T.InsDownLine(str);

  // Act & assert
  T.GoFirstLink();
  EXPECT_EQ(T.GoDownLink(), Data::TextOk);
}

TEST(TText, can_del_down_line)
{
  // Arg
  const string str1 = "Hello world!";
  const string str2 = "Hello!";
  const string str3 = "World welcomes you";
  TTextLink::InitMemSystem(4);
  TText T;
  
  T.GoFirstLink();
  T.InsDownLine(str1);
  T.GoDownLink();
  T.InsDownLine(str2);
  T.InsDownLine(str3);

  // Act
  T.GoDownLink();
  T.DelDownLine();

  // Assert
  T.GoFirstLink();
  T.GoDownLink();
  EXPECT_EQ(T.GetLine(), str1);
  T.GoDownLink();
  EXPECT_EQ(T.GetLine(), str3);
}

TEST(TText, can_del_down_section)
{
  // Arg
  const string sec1 = "section 1";
  const string sec2 = "section 1.1";
  const string sec3 = "section 1.2";
  const string str1 = "string 1";
  const string str2 = "string 2";
  TTextLink::InitMemSystem(6);
  TText T;

  T.GoFirstLink();
  T.InsDownLine(sec1);
  T.GoDownLink();
  T.InsDownLine(sec3);
  T.InsDownLine(sec2);
  T.GoDownLink();
  T.InsDownLine(str2);
  T.InsDownLine(str1);

  // Act
  T.DelDownSection();

  // Assert
  T.GoFirstLink();
  T.GoDownLink();
  T.GoDownLink();
  EXPECT_EQ(T.GoDownLink(), Data::TextNoDown);
}

TEST(TText, can_del_next_line)
{
  // Arg
  const string sec1 = "section 1";
  const string str1 = "string 1";
  const string str2 = "string 3";
  const string str3 = "string 2";
  TTextLink::InitMemSystem(5);
  TText T;

  T.GoFirstLink();
  T.InsDownLine(sec1);
  T.GoDownLink();
  T.InsDownSection(str1);
  T.GoDownLink();
  T.InsNextLine(str2);
  T.InsNextLine(str3);

  // Act
  T.DelNextLine();

  // Assert
  T.GoFirstLink();
  T.GoDownLink();
  T.GoDownLink();
  EXPECT_EQ(T.GetLine(), str1);
  T.GoNextLink();
  EXPECT_EQ(T.GetLine(), str2);
}

TEST(TText, can_del_next_section)
{
  // Arg
  const string sec1 = "section 1";
  const string sec2 = "section 2";
  const string str1 = "string 1";
  const string str2 = "string 2";
  const string str3 = "string 3";
  TTextLink::InitMemSystem(7);
  TText T;

  T.GoFirstLink();
  T.InsDownLine(sec1);
  T.GoDownLink();
  T.InsNextLine(str2);
  T.InsNextLine(str1);
  T.InsNextSection(sec2);
  T.InsNextLine(str3);

  T.Write("dewv.txt");
  // Act
  T.GoNextLink();
  T.DelNextSection();

  // Assert
  T.GoFirstLink();
  T.GoDownLink();
  T.GoNextLink();
  EXPECT_EQ(T.GetLine(), str3);
}